package com.craterzone.california;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by mohdhaseen on 7/5/15.
 */
public class MessageReceiver extends BroadcastReceiver {
    private static final String TAG="MessageReceiver";
    @Override
    public void onReceive(Context context,Intent intent){

        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle pudsBundle = intent.getExtras();
            Object[] pdus = (Object[]) pudsBundle.get("pdus");
            SmsMessage sms =SmsMessage.createFromPdu((byte[]) pdus[0]);
            Log.i(TAG, sms.getMessageBody());
            Log.i(TAG,sms.getOriginatingAddress());
            Toast.makeText(context,"A new message Received ",Toast.LENGTH_LONG).show();
        }
    }
}
