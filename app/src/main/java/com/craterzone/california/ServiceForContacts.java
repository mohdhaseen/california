package com.craterzone.california;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.widget.Toast;

/**
 * Created by mohdhaseen on 6/5/15.
 */
public class ServiceForContacts extends Service {
    final Uri SMS_STATUS_URI = Uri.parse("content://sms");
    ContactObserver contactObserver=new ContactObserver(new Handler(),this);
    MessageObserver messageObserver=new MessageObserver(new Handler(),this);
    @Override
    public IBinder onBind(Intent intent) {
        return null;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();
        getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true,
                contactObserver);
        getContentResolver().registerContentObserver(SMS_STATUS_URI,true,messageObserver);


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service destroyed", Toast.LENGTH_LONG).show();
        getContentResolver().unregisterContentObserver(contactObserver);
        getContentResolver().unregisterContentObserver(messageObserver);
            }

}
