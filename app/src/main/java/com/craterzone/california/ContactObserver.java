package com.craterzone.california;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by mohdhaseen on 6/5/15.
 */
public class ContactObserver extends ContentObserver {
//private OnContactChangeListener onContactChangeListener;
Context c;
    public ContactObserver(Handler handler,Context context) {
        super(handler);
c=context;
    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
        Log.e("", "~~~~~~" + selfChange);

    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {

        Log.e("OnChangeUri", "~~~~~~" + selfChange);
        Toast.makeText(c,"Contact Change event occurs",Toast.LENGTH_SHORT).show();
  //      if (onContactChangeListener != null)
    //        onContactChangeListener.onContactChanged(uri);
    }
   public void setOnContactChangeListener(OnContactChangeListener onContactChangeListener) {
   //     this.onContactChangeListener = onContactChangeListener;
    }
    }

