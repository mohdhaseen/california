package com.craterzone.california;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.widget.Toast;

/**
 * Created by mohdhaseen on 7/5/15.
 */
public class MessageObserver extends ContentObserver {
    Context c;
    public MessageObserver(Handler handler,Context context) {
        super(handler);
        c=context;
    }

    @Override
    public void onChange(boolean selfChange) {
        onChange(selfChange, null);

    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
       // super.onChange(selfChange, uri);

        /*Cursor cursor = c.getContentResolver().query(
                Uri.parse("content://sms"), null, null, null, null);*/
        Cursor cursor1 = c.getContentResolver().query(
                uri, null, null, null, null);
       /* if (cursor.moveToNext()) {
            String protocol = cursor.getString(cursor.getColumnIndex("protocol"));
            int type = cursor.getInt(cursor.getColumnIndex("type"));
            // Only processing outgoing sms event & only when it
            // is sent successfully (available in SENT box).
                Toast.makeText(c, type+"", Toast.LENGTH_SHORT).show();

        }*/
        if (cursor1.moveToNext()) {
            // String protocol = cursor.getString(cursor.getColumnIndex("protocol"));
            int type = cursor1.getInt(cursor1.getColumnIndex("type"));
            // Only processing outgoing sms event & only when it
            // is sent successfully (available in SENT box).
//type = 3 for draft,1 for incomming,
            if (type==6)
            Toast.makeText(c, type+"sending", Toast.LENGTH_SHORT).show();
            if (type==4)
                Toast.makeText(c, type+"sent", Toast.LENGTH_SHORT).show();
            if (type==2)
                Toast.makeText(c, type + "delivered", Toast.LENGTH_SHORT).show();
        }



    }
}
