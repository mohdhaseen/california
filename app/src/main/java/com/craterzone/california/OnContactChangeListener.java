package com.craterzone.california;


import android.net.Uri;

/**
 * Created by mohdhaseen on 6/5/15.
 */
public interface OnContactChangeListener {
    public void onContactChanged( Uri uri);
}
